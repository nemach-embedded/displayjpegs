ESP 32 code to display jpeg files from SPIFF. 

Runs on TTGO-T-Display module. Other modules YMMV.

Dependencies

https://github.com/Bodmer/JPEGDecoder

https://github.com/Bodmer/TFT_eSPI

https://github.com/LennartHennigs/Button2


jpeg files must be pre-loaded to SPIFF using something like

https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/tag/1.0

