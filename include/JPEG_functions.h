/*====================================================================================
  This sketch contains support functions to render the Jpeg images.

  Created by Bodmer 15th Jan 2017
  ==================================================================================*/

#pragma once

void drawJpeg(TFT_eSPI& tft, const char *filename, int xpos, int ypos);
void jpegRender(TFT_eSPI& tft, int xpos, int ypos);
void jpegInfo();
void createArray(const char *filename);
