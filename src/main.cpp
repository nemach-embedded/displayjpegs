
/*====================================================================================
  This sketch contains a quick test app for the TTGO-T-Display that allows jpg images
  to be rendered from SPIFF memory.

  Created by nemach 18.05.2020
  ==================================================================================*/


#include <Arduino.h>
#include <TFT_eSPI.h>
#include <SPI.h>
#include <Wire.h>
#include <Button2.h>
#include "FS.h"
#include "SPIFFS.h"
#include "JPEG_functions.h"

#ifndef TFT_DISPOFF
#define TFT_DISPOFF 0x28
#endif

#ifndef TFT_SLPIN
#define TFT_SLPIN   0x10
#endif

#define TFT_MOSI            19
#define TFT_SCLK            18
#define TFT_CS              5
#define TFT_DC              16
#define TFT_RST             23

#define TFT_BL          4   // Display backlight control pin
#define ADC_EN          14  //ADC_EN is the ADC detection enable port
#define ADC_PIN         34
#define BUTTON_1        35
#define BUTTON_2        0

TFT_eSPI tft = TFT_eSPI(135, 240); // Invoke custom library
Button2 btn1(BUTTON_1);
Button2 btn2(BUTTON_2);

char jpegList[10][25];
int numFiles = 0;

//! Long time delay, it is recommended to use shallow sleep, which can effectively reduce the current consumption
void espDelay(int ms)
{
    esp_sleep_enable_timer_wakeup(ms * 1000);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_ON);
    esp_light_sleep_start();
}

void change_colour() {

    static int nextColour = 0;
    switch (nextColour){
      case 0: 
        tft.fillScreen(TFT_BLACK);
        break;
      case 1: 
        tft.fillScreen(TFT_RED);
        break;
      case 2: 
        tft.fillScreen(TFT_GREEN);
        break;
      case 3: 
        tft.fillScreen(TFT_BLUE);
        break;
      case 4: 
        tft.fillScreen(TFT_WHITE);
        nextColour = 0;
        break;
    }
    nextColour++;
}

void initFiles() 
{
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  Serial.write("Total bytes: ");
  Serial.println(SPIFFS.totalBytes());
  
  Serial.write("Used bytes:  ");
  Serial.println(SPIFFS.usedBytes());
  
  fs::File file = SPIFFS.open("/"); // Root directory
  String  line = "=====================================";

  Serial.println(line);
  Serial.println("  File name               Size");
  Serial.println(line);

  numFiles = 0;
  fs::File nextFile;
  while (nextFile = file.openNextFile()) {
    const char* fileName = nextFile.name();
    strcpy(jpegList[numFiles], fileName);
    Serial.print(fileName);
    int spaces = 25 - strlen(fileName); // Tabulate nicely
    while (spaces--) Serial.print(" ");
    Serial.print(nextFile.size()); Serial.println(" bytes");
    nextFile.close();
    numFiles++;
  }

  Serial.println("Check file names:");
  Serial.print("Num files = "); Serial.println(numFiles);
    
}

void displayNextFile() 
{
  tft.setRotation(0);  // portrait

  static int lastFile = 0;
  int newFile;
  do {
    newFile = random(numFiles);
  } while (newFile == lastFile);
  lastFile = newFile;

  drawJpeg(tft, jpegList[newFile], 0, 0);
}

void button_init()
{
    btn1.setPressedHandler([](Button2 & b) {
      Serial.println("btn 1 pressed - changing screen colour");
      change_colour();
    });

    btn2.setPressedHandler([](Button2 & b) {
      Serial.println("btn 2 pressed - displaying next file");
      displayNextFile();
    });
}

void button_loop()
{
    btn1.loop();
    btn2.loop();
}

void setup()
{
    Serial.begin(115200);
    Serial.println("Start");

    /*
    ADC_EN is the ADC detection enable port
    If the USB port is used for power supply, it is turned on by default.
    If it is powered by battery, it needs to be set to high level
    */
    pinMode(ADC_EN, OUTPUT);
    digitalWrite(ADC_EN, HIGH);

    tft.init();
    tft.setRotation(1);
    tft.fillScreen(TFT_BLACK);
    tft.setTextSize(2);
    tft.setTextColor(TFT_GREEN);
    tft.setCursor(0, 0);
    tft.setTextDatum(MC_DATUM);
    tft.setTextSize(1);

    if (TFT_BL > 0) { // TFT_BL has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
        pinMode(TFT_BL, OUTPUT); // Set backlight pin to output mode
        digitalWrite(TFT_BL, TFT_BACKLIGHT_ON); // Turn backlight on. TFT_BACKLIGHT_ON has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
    }

    tft.setSwapBytes(true);
    tft.setRotation(0);
    tft.fillScreen(TFT_RED);
    espDelay(333);
    tft.fillScreen(TFT_BLUE);
    espDelay(333);
    tft.fillScreen(TFT_GREEN);
    espDelay(333);

    button_init();
    initFiles();

    tft.fillScreen(TFT_BLACK);
    tft.setTextDatum(MC_DATUM);
    tft.drawString("LeftButton:", tft.width() / 2, tft.height() / 2 - 16);
    tft.drawString("Show picture", tft.width() / 2, tft.height() / 2 );
    tft.drawString("RightButton:", tft.width() / 2, tft.height() / 2 + 16);
    tft.drawString("Show colour", tft.width() / 2, tft.height() / 2 + 32 );
    tft.setTextDatum(TL_DATUM);
}

void loop()
{
    button_loop();
}
